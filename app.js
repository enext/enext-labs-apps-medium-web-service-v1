const express = require('express');
require('express-async-errors'); // Importado apenas para suportar async/await com express (https://www.npmjs.com/package/express-async-errors)
const path = require('path');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const morgan = require('morgan');
const {waitForMongoWrapper} = require('webcore/utils/mongodbUtils');
const handleNotFoundErrorsMiddleware = require('webcore/middlewares/handleNotFoundErrorsMiddleware');
const handleAnyErrorMiddleware = require('webcore/middlewares/handleAnyErrorMiddleware');
const checkJwtMiddleware = require('webcore/middlewares/checkJwtMiddleware');

const app = express();

// TODO: Gerar o log do morgan no mesmo padrão do winston
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cors());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(checkJwtMiddleware());

// Routes
app.use(require('./src/components/publication/routes/publicationRoutesV1'));

// Error handlers
app.use(handleNotFoundErrorsMiddleware);
app.use(handleAnyErrorMiddleware);

waitForMongoWrapper(async () => {
  // Initializations that depends of mongodb
});

module.exports = app;
