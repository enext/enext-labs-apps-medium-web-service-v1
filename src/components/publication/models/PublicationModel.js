const mongoose = require('mongoose');
const modelUtils = require('webcore/utils/modelUtils');
const semanticId = require('webcore/schema/types/semanticIdSchamaType');

// Schema
const PublicationSchema = new mongoose.Schema({
  _id: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    auto: true,
    description: 'Id da publicação.',
  },
  semanticId,
  name: {
    type: String,
    required: true,
    unique: true,
    maxlength: 128,
    description: 'Name da publicação.',
  },
  description: {
    type: String,
    required: false,
    unique: false,
    maxlength: 1024,
    description: 'Descrição da publicação.',
  }
}, {
  timestamps: true,
});

// Indexes
PublicationSchema.index({createdAt: 1});
PublicationSchema.index({createdAt: -1});
PublicationSchema.index({updatedAt: 1});
PublicationSchema.index({updatedAt: -1});

// Protected attributes on update
const updateProtectedAttributes = [
];

// Attributes that can be used on sort
const sortableAttributes = [
  'createdAt',
  'updatedAt',
];

module.exports = modelUtils.buildFromSchema({
  Schema: PublicationSchema,
  modelName: 'Publication',
  updateProtectedAttributes,
  sortableAttributes,
});
