const express = require('express');
const router = new express.Router();
const BasicControllerMethodsEnum = require('webcore/enums/BasicControllerMethodsEnum');

const publicationControllerV1 = require('../controllers/publicationControllerV1');

/**
 * @api {post} /api/v1/publications createPublication
 * @apiName createPublication
 * @apiVersion 1.0.0
 * @apiGroup Publications
 * @apiPermission CREATE_ANY_ORGANIZATION_ROLE
 *
 * @apiUse AuthorizationHeader
 *
 * @apiSchema (Request Body) {jsonschema=docs/api/json_schemas/PublicationRequestSchema.json} apiParam
 *
 * @apiSchema (Success Response Body) {jsonschema=docs/api/json_schemas/PublicationResponseSchema.json} apiSuccess
 */
router.post('/api/v1/publications', ...publicationControllerV1.getMiddlewares(BasicControllerMethodsEnum.CREATE));

module.exports = router;
