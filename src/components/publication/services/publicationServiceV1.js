const PublicationModel = require('../models/PublicationModel');
const BaseCrudService = require('webcore/services/BaseCrudService');
// const log = require('webcore/utils/logUtils');
// const nconf = require('nconf');

/**
 * Classe de serviço relacionada ao recurso Publication.
 * @extends BaseCrudService
 */
class PublicationServiceV1 extends BaseCrudService {}

const serviceInstance = new PublicationServiceV1({
  Model: PublicationModel,
});

module.exports = serviceInstance;
