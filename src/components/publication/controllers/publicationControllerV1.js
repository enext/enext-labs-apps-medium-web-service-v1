const ResourceBaseController = require('webcore/controllers/ResourceBaseController');
const publicationServiceV1 = require('../services/publicationServiceV1');
const publicationPermissionsV1 = require('../permissions/publicationPermissionsV1');

/**
 * Classe de serviço relacionada ao recurso Publication.
 *
 * @extends ResourceBaseController
 */
class PublicationControllerV1 extends ResourceBaseController {
  /**
   * @constructor
   * @param {Object} params
   * @param {publicationServiceV1} params.resourceService
   * @param {publicationPermissionsV1} params.resourcePermissions
   */
  constructor({
    resourceService,
    resourcePermissions,
  }) {
    console.log('chamado');
    super({
      resourceService,
      resourcePermissions,
    });
  }

  /**
   * @override
   */
  init() {
    super.init();
  }
}

const controllerInstance = new PublicationControllerV1({
  resourceService: publicationServiceV1,
  resourcePermissions: publicationPermissionsV1,
});

controllerInstance.init();

module.exports = controllerInstance;
