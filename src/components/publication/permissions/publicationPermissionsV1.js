const BasePermissions = require('webcore/permissions/BasePermissions');
const RolesEnum = require('../../../appcore/components/roles/enums/RolesEnum');
const ac = require('webcore/utils/accessControlUtils');
const BasicControllerMethodsEnum = require('webcore/enums/BasicControllerMethodsEnum');

const CREATE_ANY_ORGANIZATION_ROLE = 'CREATE_ANY_ORGANIZATION_ROLE';
const GET_MY_ORGANIZATION_ROLE = 'GET_MY_ORGANIZATION_ROLE';
const GET_ANY_ORGANIZATION_ROLE = 'GET_ANY_ORGANIZATION_ROLE';
const UPDATE_MY_ORGANIZATION_ROLE = 'UPDATE_MY_ORGANIZATION_ROLE';
const UPDATE_ANY_ORGANIZATIONS_ROLE = 'UPDATE_ANY_ORGANIZATIONS_ROLE';
const DELETE_MY_ORGANIZATION_ROLE = 'DELETE_MY_ORGANIZATION_ROLE';
const DELETE_ALL_ORGANIZATIONS_ROLE = 'DELETE_ALL_ORGANIZATIONS_ROLE';
const ORGANIZATIONS_MASTER_ROLE = 'ORGANIZATIONS_MASTER_ROLE';

/**
 * Classe responsável por definir as permissões de acesso das rotas de Publication.
 *
 * @extends BasePermissions
 */
class PublicationPermissions extends BasePermissions {
  /**
   * @constructor
   */
  constructor() {
    super();
    this.RESOURCE_NAME = 'ORGANIZATION';
  }

  /**
   * @override
   */
  getGrants() {
    return [
      {
        role: CREATE_ANY_ORGANIZATION_ROLE,
        resource: this.RESOURCE_NAME,
        action: BasicControllerMethodsEnum.CREATE,
        attributes: ['*'],
      },
      {
        role: GET_MY_ORGANIZATION_ROLE,
        resource: this.RESOURCE_NAME,
        action: [BasicControllerMethodsEnum.GET],
        attributes: ['*'],
        condition: {
          Fn: 'EQUALS',
          args: {
            requesterPublicationId: '$.requestedPublicationId',
          },
        },
      },
      {
        role: GET_ANY_ORGANIZATION_ROLE,
        resource: this.RESOURCE_NAME,
        action: [BasicControllerMethodsEnum.GET, BasicControllerMethodsEnum.LIST],
        attributes: ['*'],
      },
      {
        role: UPDATE_MY_ORGANIZATION_ROLE,
        resource: this.RESOURCE_NAME,
        action: BasicControllerMethodsEnum.UPDATE,
        attributes: ['*'],
        condition: {
          Fn: 'EQUALS',
          args: {
            requesterPublicationId: '$.requestedPublicationId',
          },
        },
      },
      {
        role: UPDATE_ANY_ORGANIZATIONS_ROLE,
        resource: this.RESOURCE_NAME,
        action: BasicControllerMethodsEnum.UPDATE,
        attributes: ['*'],
      },
      {
        role: DELETE_MY_ORGANIZATION_ROLE,
        resource: this.RESOURCE_NAME,
        action: BasicControllerMethodsEnum.DELETE,
        attributes: ['*'],
        condition: {
          Fn: 'EQUALS',
          args: {
            requesterPublicationId: '$.requestedPublicationId',
          },
        },
      },
      {
        role: DELETE_ALL_ORGANIZATIONS_ROLE,
        resource: this.RESOURCE_NAME,
        action: [BasicControllerMethodsEnum.DELETE],
        attributes: ['*'],
      },
      {
        role: ORGANIZATIONS_MASTER_ROLE,
        resource: this.RESOURCE_NAME,
        action: [
          BasicControllerMethodsEnum.CREATE,
          BasicControllerMethodsEnum.GET,
          BasicControllerMethodsEnum.LIST,
          BasicControllerMethodsEnum.UPDATE,
          BasicControllerMethodsEnum.DELETE],
        attributes: ['*'],
      },
    ];
  }

  /**
   * @override
   */
  getExtends() {
    return [
      {
        role: RolesEnum.ENEXT_ROOT_ROLE,
        childrenRoles: [
          ORGANIZATIONS_MASTER_ROLE,
        ],
      },
      {
        role: RolesEnum.ENEXT_ADMIN_ROLE,
        childrenRoles: [
          CREATE_ANY_ORGANIZATION_ROLE,
          GET_ANY_ORGANIZATION_ROLE,
        ],
      },
      {
        role: RolesEnum.ENEXT_STAFF_ROLE,
        childrenRoles: [
          GET_ANY_ORGANIZATION_ROLE,
        ],
      },
      {
        role: RolesEnum.EXTERNAL_ORGANIZATION_ADMIN_ROLE,
        childrenRoles: [
          GET_MY_ORGANIZATION_ROLE,
          UPDATE_MY_ORGANIZATION_ROLE,
        ],
      },
      {
        role: RolesEnum.EXTERNAL_ORGANIZATION_STAFF_ROLE,
        childrenRoles: [
          GET_MY_ORGANIZATION_ROLE,
        ],
      },
    ];
  }
}

const resourcePermissions = new PublicationPermissions();

resourcePermissions.init(ac);

module.exports = resourcePermissions;
